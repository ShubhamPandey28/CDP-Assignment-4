#include <bits/stdc++.h>
#include "Transaction.hpp"
#include <thread>

using namespace std;
using namespace assignment4;

template<typename T>
void print(vector<T> v){
    for (auto i : v) cout << i << " ";
    cout << endl;
}

template<typename T>
void print2d(vector<vector<T>> v){
    for (auto i : v) print(i);
}

map<string,int> read_variable_values(){
    // number of variables
    int n;
    cin >> n;

    // read initialised varialbles
    map<string,int> vars;     // map to store intitialised values
    string var;
    int val;
    string s;

    for(int i=0;i<n;i++){
        cin >> var;         // get variable name
        cin >> val;         // get variable value
        vars[var]=val;
    }

    return vars;
}

int main(){
    map<string,int> database = read_variable_values();
    TransactionManager TM;
    int t;
    cin >> t;

    for(int i=0;i<t;i++){
        Transaction T;
        cin >> T.id;  // assign id
        string temp;
        string tmp2;
        while(getline(cin>>ws, temp)){
            vector<string> tmp;
            if(temp[0]=='C' || temp[0]=='A'){
                tmp2=temp[0];
                tmp.push_back(tmp2);
                T.operations.push_back(tmp);// push outcome
                break;
            }
            else if(temp[0]=='R' || temp[0]=='W'){
                tmp2=temp[0];
                tmp.push_back(tmp2);             //push R or W
                tmp2=temp[2];
                tmp.push_back(tmp2);             //push variable name
                
            }
            else{
                
                if(temp[3]=='+')
                {
                    tmp2="+";
                    tmp.push_back(tmp2);         
                    tmp2=temp[0];
                    tmp.push_back(tmp2);
                    tmp2=temp[0];
                    tmp.push_back(tmp2);
                    tmp2=temp.substr(4,temp.length()-4);
                    tmp.push_back(tmp2);
                }
                else if(temp[3]=='-')
                {
                    tmp2="-";
                    tmp.push_back(tmp2);
                    tmp2=temp[0];
                    tmp.push_back(tmp2);
                    tmp.push_back(tmp2);
                    tmp2=temp.substr(4,temp.length()-4);
                    tmp.push_back(tmp2);
                }
                else
                {
                    tmp.push_back("+");
                    tmp2=temp[0];
                    tmp.push_back(tmp2);
                    tmp.push_back(tmp2);
                    tmp2=temp[4];
                    tmp.push_back(tmp2);
                }
                
            }
            T.operations.push_back(tmp);
        }
        TM.transactions[T.id] = T;
    }
    LockManager LM(database);
    TM.execute(LM);
    for (auto i: database){
        cout<<i.first<<": "<<LM.variables[i.first]->value<<"\n";
    }
    // cout << LM.variables.size() << endl;
    // cout << LM.lock_order.size() << endl;
    // cout<<LM.lock_order->size()<<"\n";
    // auto i=LM.lock_order->begin();
    // while(i!=LM.lock_order->end()){
    //     cout << i->first << " " << i->second.first << " " << i->second.second << endl; 
    //     i++;
    // }
}
