#include <bits/stdc++.h>

using namespace std;


namespace assignment4
{
    enum LockMode {
        UNLOCKED = 0,
        SHARED = 1,
        EXCLUSIVE = 2,
    };

    class StateVar {
        public:
        string name;
        int value;
        LockMode mode = UNLOCKED;
        mutex mtx;
        condition_variable cv_read, cv_write;
        set<int> s_wait, x_wait, s_active; // contains tx.id for the respective
        int x_active = -1;
        StateVar(string c, int v) { value = v; name = c;}
        StateVar(pair<string, int > p) {name=p.first;value=p.second; }
        void print_status(){
            string out = "s_wait: ";
            for (auto i : s_wait) out += to_string(i)+" ";
            out += "\nx_wait: ";
            for (auto i : x_wait) out += to_string(i) +" ";
            out += "\ns_active: ";
            for (auto i : s_active) out += to_string(i) +" ";
            out += "\nx_active : " + to_string(x_active);
            cout << out << endl;
        }
    };

    class Transaction {
        public:
        vector< vector<string> > operations; // <varname, < read / write, value (if write) >>
        set<string> variables;
        int id;
        int outcome=0; // running 0, abort -1, commited 1
         // return outcome;
    };

    class LockManager {
        public:
        map<string, StateVar* > variables; // var name -> Transaction id using it default -1 (none)
        // vector<pair<string, pair<int, string>>>* lock_order; // <type, <tx_id, variable>>
        LockManager(map<string, int> database){
            for (auto i : database) 
                variables[i.first] = new StateVar(i);
            
            
        }
        void acquire_write_lock(int tx_id, string variable); // 1 when not given, 0 when succesfully given
        void acquire_read_lock(int tx_id, string variable);
        void upgrade_to_write(int tx_id, string variable);
        void release_lock(int tx_id, string variable);
        void release_all(Transaction t);
    };

    

    //void execute_tx(Transaction t, LockManager LM,map<string, int> database);

    class TransactionManager {
        private:
        int NEW_TX_ID=0;
        public:
        map<int, Transaction > transactions; // id -> Transaction object
        Transaction create_transaction(){
            Transaction t;
            t.id = NEW_TX_ID;
            transactions[t.id] = t;
            NEW_TX_ID++;
            return t;
        }
        void execute(LockManager LM);
    };
}
