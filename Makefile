SRC = Transaction.cpp main.cpp
EXEC = main

all:
	g++ -pthread -o $(EXEC) $(SRC)

test:
	cat test_inputs/input1.txt | ./main > output1.txt && cat output1.txt
	cat test_inputs/input2.txt | ./main > output2.txt && cat output2.txt
	cat test_inputs/input4a.txt | ./main > output4a.txt && cat output4a.txt
	cat test_inputs/input4b.txt | ./main > output4b.txt && cat output4b.txt
	cat test_inputs/input5.txt | ./main > output5.txt && cat output5.txt

clean:
	rm main
