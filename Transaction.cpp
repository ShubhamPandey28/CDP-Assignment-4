#include <bits/stdc++.h>
#include "Transaction.hpp"

using namespace std;
using namespace assignment4;


void LockManager::release_lock(int tx_id, string variable){
    StateVar* state = variables[variable];
    unique_lock<mutex> ul(state->mtx);
    if (state->s_active.count(tx_id)){
        state->s_active.erase(tx_id);
        if (state->s_active.size() == 0){
            state->mode = UNLOCKED;
            if (state->x_wait.size()) state->cv_write.notify_all();
        }
        else if (state->s_wait.size()) state->cv_read.notify_all();
        printf("UNLOCKED -> transaction_id: %d variable_name: %s\n", tx_id, variable.c_str());
    }
    else if( state->x_active == tx_id ){
        state->x_active = -1;
        state->mode= UNLOCKED;
        if (state->s_wait.size()) state->cv_read.notify_all();
        else if (state->x_wait.size()) state->cv_write.notify_all();
        printf("UNLOCKED -> transaction_id: %d variable_name: %s\n", tx_id, variable.c_str());
    }
    ul.unlock();
}

void LockManager::acquire_read_lock(int tx_id, string variable){
    // cout<<"Reading..."<<"\n";
    StateVar* state = variables[variable];
    unique_lock<mutex> ul(state->mtx);

    while (state->mode == EXCLUSIVE){
        state->s_wait.insert(tx_id);
        state->cv_read.wait(ul);
        state->s_wait.erase(tx_id);
    }
    state->s_active.insert(tx_id);
    pair<int, string> p = {tx_id,variable};
    printf("SHARED -> transaction_id: %d variable_name: %s\n", tx_id, variable.c_str());
    // lock_orderpush_back({"Shared", p});
    // cout<<lock_order->size()<<"\n";
    if (state->s_wait.size() == 0) state->mode = UNLOCKED;
    else state->mode = SHARED;
    ul.unlock();
}

void LockManager::acquire_write_lock(int tx_id, string variable){
    // cout<<"Writing..."<<"\n";
    StateVar *state = variables[variable];
    unique_lock<mutex> ul(state->mtx);

    while(state->mode != UNLOCKED){
        state->x_wait.insert(tx_id);
        state->cv_write.wait(ul);
        state->x_wait.erase(tx_id);
    }

    state->mode = EXCLUSIVE;
    state->x_active = tx_id;
    pair<int, string> p = {tx_id,variable};

    printf("EXCLUSIVE -> transaction_id: %d variable_name: %s\n", tx_id, variable.c_str());
    // lock_order->push_back({"Exclusive", p});
    // cout << lock_order->size() << endl;
    ul.unlock();
}

void LockManager::upgrade_to_write(int tx_id, string variable){
    StateVar *state = variables[variable];
    unique_lock<mutex> ul(state->mtx);

    while(state->mode != UNLOCKED){
        state->x_wait.insert(tx_id);
        state->cv_write.wait(ul);
        state->x_wait.erase(tx_id);
    }

    if (state->s_active.count(tx_id)){
        state->s_active.erase(tx_id);
        if (state->s_active.size() == 0){
            state->mode = UNLOCKED;
            if (state->x_wait.size()) state->cv_write.notify_one();
        }
        else if (state->s_wait.size()) state->cv_read.notify_all();
    }

    state->x_active = tx_id;
    pair<int, string> p = {tx_id,variable};
    printf("UPGRADE -> transaction_id: %d variable_name: %s\n", tx_id, variable.c_str());
    // lock_order->push_back({"Upgrade", p});
    // cout << lock_order->size() << endl;
    ul.unlock();
}

void LockManager::release_all(Transaction t){
    cout << "called release" << endl;
    for (auto i : t.variables){
        
        /*
        cout << "i " + i << endl;
        StateVar *state = variables[i];
        state->print_status();
        state->mtx.lock();
        if (state->s_active.count(t.id))
        {
            state->s_active.erase(t.id);
            if (state->s_active.size() == 0){
                state->mode = UNLOCKED;
                if (state->x_wait.size()) state->cv_write.notify_all();
            }
            else if (state->s_wait.size()) state->cv_read.notify_all();
            printf("UNLOCKED -> transaction_id: %d variable_name: %s\n", t.id, i.c_str());
        }
        else if( state->x_active == t.id ){
            state->x_active = -1;
            state->mode= UNLOCKED;
            if (state->s_wait.size()) state->cv_read.notify_all();
            else if (state->x_wait.size()) state->cv_write.notify_all();
            printf("UNLOCKED -> transaction_id: %d variable_name: %s\n", t.id, i.c_str());
        }
        state->mtx.unlock();
        */
       release_lock(t.id, i);
    }
}


void execute_tx(Transaction t,LockManager LM){
    map<string, int> buffer;
    // for(auto i:t.operations){
    //     for(auto j:i){
    //         cout<<j<<" ";
    //     }
    //     cout<<"\n";
    // }
    for (auto i : t.operations){
        if (i[0] == "R"){
            LM.acquire_read_lock(t.id, i[1]);
        }
        else if (i[0] == "W"){
            LM.acquire_write_lock(t.id, i[1]);
        }
        else if (i[0] == "+"){
            if (buffer.count(i[1]) == 0){
                buffer[i[1]] = LM.variables[i[1]]->value;
            }
            if (LM.variables.count(i[3])==0){
                buffer[i[1]] = LM.variables[i[2]]->value + stoi(i[3]);
            }
            else{
                buffer[i[1]] = LM.variables[i[2]]->value + LM.variables[i[3]]->value;
            }
        }
        else if (i[0] == "-"){
            if (buffer.count(i[1]) == 0){
                buffer[i[1]] = LM.variables[i[1]]->value;
            }
            if (LM.variables.count(i[3])==0){
                buffer[i[1]] = LM.variables[i[2]]->value - stoi(i[3]);
            }
            else{
                buffer[i[1]] = LM.variables[i[2]]->value - LM.variables[i[3]]->value;
            }
        }
        else if (i[0]=="C")
        {
            for (auto it : buffer)
                LM.variables[it.first]->value = it.second;
            LM.release_all(t);
        }
        else if (i[0]=="A") LM.release_all(t); 
    }
}


void TransactionManager::execute(LockManager LM){
    vector<thread> threads;
    for (auto i : transactions)
    {
        Transaction temp = i.second;
        thread t(execute_tx,temp, LM);
        threads.push_back(move(t));
    }
    for (auto &t : threads){
        t.join();
    }
}

/*
void execute_txs(TransactionManager TM,LockManager LM, map<string, int> database){
    vector<thread> threads;
    for (auto i : TM.transactions)
    {
        thread t(execute_tx,i.second, LM, database);
        threads.push_back(t);
    }
    for (auto &t : threads){
        t.join();
    }
}
*/