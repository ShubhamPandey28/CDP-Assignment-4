# CDP-Assignment-4
Implementation of Rigorous 2 phase locking protocol as an assignment in course CS310 (Communicating distributed Processes).

### Running

- build : ```make``` <br>
- test : ```make test``` <br>
